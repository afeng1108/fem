
BUILD_DIR := $(PWD)
KERNELPATH ?= $(DIR_LINUX)

BUILD_FLAGS := \
	M=$(BUILD_DIR) \
	PWD=$(BUILD_DIR) \
	INSTALL_MOD_DIR=updates/ -d

obj-m := fastbat.o

all:
	$(MAKE) -C  $(KERNELPATH) $(BUILD_FLAGS) modules

install:
	$(MAKE) -C $(KERNELPATH) $(BUILD_FLAGS) modules_install

clean:
	$(RM) *.o *.ko

.PHONY: all install clean

