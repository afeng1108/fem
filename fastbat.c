#include "fastmesh.h"

#define FASTBAT_MASSAGE_RATE_LIMIT		(3*HZ)
#define FASTBAT_WARN_RATE_LIMIT			(3*HZ)
#define FASTBAT_ERR_RATE_LIMIT			(1*HZ)

DEFINE_RATELIMIT_STATE(fastbat_message_state, FASTBAT_MASSAGE_RATE_LIMIT, 2);
DEFINE_RATELIMIT_STATE(fastbat_warn_state, FASTBAT_WARN_RATE_LIMIT, 2);
DEFINE_RATELIMIT_STATE(fastbat_bug_state, FASTBAT_WARN_RATE_LIMIT, 3);
DEFINE_RATELIMIT_STATE(fastbat_err_state, FASTBAT_WARN_RATE_LIMIT, 3);

#if	(FASTBAT_DBG==1)
#define FASTBAT_DBG_RAW_MSG(...)				printk(KERN_WARNING __VA_ARGS__)

#define FASTBAT_DBG_MSG(...)					\
do {											\
	if(__ratelimit(&fastbat_message_state))		\
		printk(KERN_WARNING "[F@STBAT]" __VA_ARGS__);	\
} while(0)
#define FASTBAT_ERR_MSG(...)					\
do {											\
	if(__ratelimit(&fastbat_err_state))			\
		printk(KERN_ERR "[F@STBAT]" __VA_ARGS__);	\
} while(0)
#else	//!FASTBAT_DBG
#define FASTBAT_DBG_RAW_MSG(...)
#define FASTBAT_DBG_MSG(...)
#define FASTBAT_ERR_MSG(...)                    printk(KERN_ERR __VA_ARGS__)
#endif

#define FASTBAT_WARN_ON(...)					\
do {											\
	if(__ratelimit(&fastbat_warn_state))		\
		WARN_ON(__VA_ARGS__);					\
} while(0)

#define FASTBAT_BUG_ON(...)						\
do {											\
	if(__ratelimit(&fastbat_bug_state))			\
		BUG_ON(__VA_ARGS__);					\
} while(0)

static DEFINE_MUTEX(bats_lock);
static DEFINE_MUTEX(local_lock);
static struct hlist_head	bats[FAST_BAT_LIST_LEN];	//record all global bats
static u8	forbid_mac_addr[][ETH_ALEN+1] = {
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 6},	//"broadcast"
	{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 6},	//"zero addr"
	{0x01, 0x80, 0xc2, 0x00, 0x00, 0x00, 5},	//"stp addr": spanning tree
	{0xcf, 0x00, 0x00, 0x00, 0x00, 0x00, 6},	//"ectp addr": ethernet v2.0 configuration testing
	{0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 1},	//"multicast": is_multicast_ether_addr(addr)
	{0x33, 0x33, 0x00, 0x00, 0x00, 0x00, 2},	//multicast of ipv6
	{NULL}, 
};
LIST_HEAD(fastbat_local_head);
struct workqueue_struct	*fastmesh_wq;
struct delayed_work fastbat_work;

static int	fastbat_clone_header(void *src, void *dst);			//clone and then xmit
static int  fastbat_xmit_wrap(struct sk_buff *, FAST_BAT *);	//wrap fastbat header
static int  fastbat_xmit(struct sk_buff *, FAST_BAT *);			
static int  fastbat_insert_list_entry(u8 *src, u8 *dst, FAST_BAT *);
static u32  fastbat_hash_calculate(u8 *src, u8 *dst);
static u8   fastbat_verdict_phyaddr(u8 *addr);					//check mac addr
static u8   fastbat_verdict_packettype(struct ethhdr *);		//check packet type
static void	fastbat_show_list_entry(u32 idx);
static FAST_BAT	*fastbat_skb_peel_and_search(struct sk_buff *);	//grab src/dst addr and search
static void	fastbat_del_list_entry(FAST_BAT *);
static void	fastbat_del_local_entry(FAST_LOCAL *);
static void	fastbat_show_local_entry(void);
static void fastbat_local_add_check_global_and_setup_dirty(u8 *addr);	//callback

/* sysctl variable to turn on/off fastbat dynamically */
extern int fastmesh_enable;
extern struct br_fastbat_ops _br_fastbat_ops;
FASTMESH  _fastmesh_priv;

/* input: src/dst mac
 *
 * return 0~(FAST_BAT_LIST_LEN-1)
 * return 0xfffffffe if failed
 * 
 * value 1: src[0] src[1] src[2] src[3]
 * value 2: dst[0] dst[1] dst[2] dst[3]
 * value 3: dst[4] src[4] dst[5] src[5]
 */
static u32	fastbat_hash_calculate
(u8 *src, 
 u8 *dst)
{
	u32	a, b, c=0, hash=0;

	if((src==NULL) || (dst==NULL))
		return	0xfffffffe;

	a=*(src)<<24 | *(src+1)<<16 | *(src+2)<<8 | *(src+3);
	b=*(dst)<<24 | *(dst+1)<<16 | *(dst+2)<<8 | *(dst+3);
	c=*(dst+4)<<24 | *(src+4)<<16 | *(dst+5)<<8 | *(src+5);
	hash=jhash_3words(a, b, c, 0);
	return	(hash%FAST_BAT_LIST_LEN);
}

/* Example:
 * u32 idx=fastbat_hash_calculate(src, dst);
 * if(idx == 0xfffffffe)
 *    //[F@STBAT] error hash value!
 * fastbat_insert_list_entry(src, dst, bat);
 * 
 * 
 * return 0 if successful
 *        -EINVAL if failed
 * input: 
 * - src: source mac
 * - dst: dest mac
 */
static int	fastbat_insert_list_entry
(u8 *src, 
 u8 *dst, 
 FAST_BAT *bat)
{
	u32	idx=fastbat_hash_calculate(src, dst);

	if(bat == NULL)
		return	-EINVAL;
	if(idx >= FAST_BAT_LIST_LEN)
		return	-EINVAL;

	/* append into global list */
	INIT_HLIST_NODE(&bat->list);
	mutex_lock(&bats_lock);
	hlist_add_head(&bat->list, &bats[idx]);
	mutex_unlock(&bats_lock);
	return	0;
}

#if (FASTBAT_DBG == 1)
/* dump all hash entry to debug
 *
 */
static void		fastbat_show_all_list_entry
(void)
{
	int			idx=0;
	FAST_BAT	*bat;

	FASTBAT_DBG_RAW_MSG("[F@STBAT] all entries:\n");
	FASTBAT_DBG_RAW_MSG("<src>  <dst>  <orig>  <dirty>  <soft>  <out>  <vlan>  <batman>  <batman-type>\n");
	for(; idx<FAST_BAT_LIST_LEN; idx++) {
		if(!hlist_empty(&bats[idx])) {
			FASTBAT_DBG_RAW_MSG("<idx-%d>\n", idx);
			hlist_for_each_entry(bat, &bats[idx], list) {
				FASTBAT_DBG_RAW_MSG("%pM %pM %pM %d %s %s %d %d %d\n", 
					bat->src, bat->dst, bat->orig, 
					bat->dirty, bat->soft_iface->name, bat->net_dev->name, bat->vlan, bat->batman, bat->batman_type);
			}
		}
	}
}

static void		fastbat_show_list_entry
(u32 idx)
{
	FAST_BAT	*bat;
	if(hlist_empty(&bats[idx])) {
		FASTBAT_DBG_RAW_MSG("[F@STBAT-%d] is NULL!!!\n", idx);
		return;
	}
	FASTBAT_DBG_RAW_MSG("[F@STBAT-%d]\n", idx);
	hlist_for_each_entry(bat, &bats[idx], list) {
		FASTBAT_DBG_RAW_MSG("(src)%pM\n (dst)%pM\n (real src)%pM\n (real dst)%pM\n (orig)%pM\n (dirty)%d (soft)%s (out)%s (vlan)%d (batman)%d (batman-type)%d\n", 
			bat->src1, bat->dst1, bat->src, bat->dst, bat->orig, 
			bat->dirty, bat->soft_iface->name, bat->net_dev->name, bat->vlan, bat->batman, bat->batman_type);
	}
}

static void		fastbat_show_local_entry
(void)
{
	FAST_LOCAL	*local;
	FASTBAT_DBG_RAW_MSG("[F@STBAT-%d]: \n");
	list_for_each_entry(local, &fastbat_local_head, list) {
		FASTBAT_DBG_RAW_MSG("(dst)%pM (dirty)%d (out)%s\n", local->dst, local->dirty, local->output_dev->name);
		
	}
}

/* input: len < 256
 */
void	fastbat_show_raw_data
(u8 *begin, 
 u8 len)
{
	u8	idx=0;
	for(idx=0 ;idx<len; idx+=16) {
		FASTBAT_DBG_RAW_MSG("[%x] %x %x %x %x %x %x %x %x   %x %x %x %x %x %x %x %x\n", 
			begin, *begin, *(begin+1), *(begin+2), *(begin+3), *(begin+4), *(begin+5), *(begin+6), *(begin+7), 
			*(begin+8), *(begin+9), *(begin+10), *(begin+11), *(begin+12), *(begin+13), *(begin+14), *(begin+15));
		begin += 16;
	}
}

#else
static void     fastbat_show_all_list_entry
(void)
{
	return;
}

static void     fastbat_show_list_entry
(u32 idx)
{
	return;
}

static void		fastbat_show_local_entry
(void)
{
	return;
}

void	fastbat_show_raw_data
(u8 *begin, 
 u8 len)
{
	return;
}
#endif	//FASTBAT_DBG

/* TODO: double vlan
 * 
 * peel outgoing skb to grab src and dst addr and search jhash index
 * return NULL if failed
 */
static FAST_BAT		*fastbat_skb_peel_and_search
(struct sk_buff *skb)
{
	FAST_BAT	*bat=NULL;
	int			hdr_len=0;
	struct ethhdr		*ethhdr=eth_hdr(skb), 
						*real_ethhdr;
	struct vlan_ethhdr	*vhdr;
	struct batadv_unicast_packet	*u;
	struct batadv_unicast_4addr_packet	*u4;

	fastbat_show_raw_data((u8 *)ethhdr, 48);
	if(ethhdr->h_proto == ETH_P_8021Q) {
		hdr_len=ETH_VLAN_HLEN;
		vhdr=vlan_eth_hdr(skb);
		if(vhdr->h_vlan_encapsulated_proto == ETH_P_BATMAN) {
			u=((struct batadv_unicast_packet *)(vhdr+1));
			if(u->packet_type == BATADV_UNICAST)
				hdr_len += sizeof(struct batadv_unicast_packet);
			else if(u->packet_type == BATADV_UNICAST_4ADDR)
				hdr_len += sizeof(struct batadv_unicast_4addr_packet);
			else
				goto	RETN;
		} else {
			FASTBAT_ERR_MSG(" Err! vlan but w/o batman header!\n");
			goto	RETN;
		}
	} else if(ethhdr->h_proto == ETH_P_BATMAN) {
		hdr_len=ETH_HLEN;
		u=((struct batadv_unicast_packet *)(ethhdr+1));
		if(u->packet_type == BATADV_UNICAST)
			hdr_len += sizeof(struct batadv_unicast_packet);
		else if(u->packet_type == BATADV_UNICAST_4ADDR)
			hdr_len += sizeof(struct batadv_unicast_4addr_packet);
		else
			goto	RETN;
	}
	real_ethhdr=(struct ethhdr *)((u8 *)ethhdr+hdr_len);
	bat=fastbat_list_search(real_ethhdr->h_source, real_ethhdr->h_dest);
RETN:
	return	bat;
}

/* search local list and then append into local list (if nonexist)
 * TODO: double vlan
 * return 0 if successful
 *        -EINVAL/-EEXIST/-ENOBUFS/-ENOMEM if else
 */
int		fastbat_chk_and_add_local_entry
(struct sk_buff *skb, 
 struct net_device *out)
{
	FAST_LOCAL	*local;
	int			retval= -EINVAL;
	struct ethhdr	*ethhdr=eth_hdr(skb);

	/*if(out == NULL) {
		FASTBAT_ERR_MSG(" output dev is NULL!!!\n");
		goto	RETN;
	}*/
	if(!fastbat_verdict_phyaddr(ethhdr->h_dest)) {
		FASTBAT_DBG_MSG(" prohibit PHY addr!!!\n");
		goto	RETN;
	}
	if(!memcmp(out->name, "bat", 3)) {
		FASTBAT_DBG_MSG(" prohibit bat iface!!!\n");
		goto    RETN;
	}
	//FASTBAT_DBG_RAW_MSG("[F@STBAT] local bat - %pM %s\n", ethhdr->h_dest, out->name);

	/* search local list */
	local=fastbat_local_search(ethhdr->h_dest);
	if(local) {
		FASTBAT_DBG_MSG("[F@STBAT] local bat's exist!\n");
		retval=-EEXIST;
		goto	RETN;
	}
	/* verdict if addr exists in local tt host, re-check local transtable to avoid wrong insert */
	if(!_br_fastbat_ops.fastbat_check_local_tt(ethhdr->h_dest)) {
		FASTBAT_DBG_RAW_MSG("[F@STBAT] non-exist in local tt - %pM \n", ethhdr->h_dest);
		retval=-EINVAL;
		goto	RETN;
	}

	/* append */
	if(atomic_read(&_fastmesh_priv.local_count) >= FAST_BAT_LOCAL_LEN_MAX) {
		retval=-ENOBUFS;
		FASTBAT_DBG_RAW_MSG("[F@STBAT] Cache size is full!!!\n");
		goto	RETN;
	}
	local=kzalloc(sizeof(FAST_LOCAL), GFP_KERNEL);
	if(local == NULL) {
		retval=-ENOMEM;
		goto	RETN;
	}
	atomic_inc(&_fastmesh_priv.local_count);
	memcpy(local->dst, ethhdr->h_dest, ETH_ALEN);
	local->last_seen=jiffies;

	/* replace storing incoming dev idx with outgoing dev idx */
	//local->iif=skb->skb_iif;	//incoming
	local->iif=out->ifindex;	//outgoing

	local->output_dev=out;
	fastbat_local_add_check_global_and_setup_dirty(local->dst);
	list_add(local, &fastbat_local_head);
RETN:
	return		retval;
}

/* search global list and then append into global list (if nonexist)
 * TODO: batadv_unicast_4addr_packet part and double vlan part
 * input - 
 *   @neigh: can be null
 * return 0 if successful
 *        -EINVAL/-ENODEV/-ENOMEM/-EEXIST/-ENOBUFS if else
 */
int		fastbat_chk_and_add_list_entry
(struct sk_buff *skb, 
 struct net_device *soft, 
 struct net_device *out, 
 u8 *neigh)
{
	FAST_BAT		*bat;
	int				retval=-EINVAL, 
					hdr_len=0;
	bool			is4addr;
	u8				packettype=0;
	struct ethhdr	*ethhdr=eth_hdr(skb), 
					*real_ethhdr;
	struct vlan_ethhdr	*vhdr;
	struct batadv_unicast_packet		*unicast, 
										*u;
	struct batadv_unicast_4addr_packet	*unicast_4addr, 
										*u4;

	/* chking */
	if(!fastbat_verdict_phyaddr(ethhdr->h_dest)) {
		//FASTBAT_DBG_MSG(" prohibit addr!!!\n");
		goto	RETN;
	}
	if((soft==NULL) || (out==NULL)) {
		FASTBAT_ERR_MSG(" soft/out dev is NULL!!!\n");
		retval=-ENODEV;
		goto    RETN;
	}

HDR_PARSE:
	if(ethhdr->h_proto == ETH_P_8021Q) {
		hdr_len=VLAN_ETH_HLEN;
		vhdr=vlan_eth_hdr(skb);
		if(vhdr->h_vlan_encapsulated_proto == ETH_P_BATMAN) {
			u=((struct batadv_unicast_packet *)(vhdr+1));
			packettype=u->packet_type;
			if(u->packet_type == BATADV_UNICAST)
				hdr_len += sizeof(struct batadv_unicast_packet);
			/*else if(u->packet_type == BATADV_UNICAST_4ADDR) {
				//hdr_len += sizeof(struct batadv_unicast_4addr_packet);
				FASTBAT_DBG_RAW_MSG("[F@STBAT] Warning! 4addr!\n");
				retval=-EINVAL;
				goto	RETN;
			} */else {
				retval=-EINVAL;
				goto	RETN;
			}
		} else {
			FASTBAT_ERR_MSG("[F@STBAT] Err! vlan but w/o batman header!\n");
			retval=-EINVAL;
			goto	RETN;
		}
	} else if(ethhdr->h_proto == ETH_P_BATMAN) {
		hdr_len=ETH_HLEN;
		u=((struct batadv_unicast_packet *)(ethhdr+1));
		packettype=u->packet_type;
		if(u->packet_type == BATADV_UNICAST)
			hdr_len += sizeof(struct batadv_unicast_packet);
		/*else if(u->packet_type == BATADV_UNICAST_4ADDR) {
			//hdr_len += sizeof(struct batadv_unicast_4addr_packet);
			FASTBAT_DBG_RAW_MSG("[F@STBAT] Warning! 4addr!\n");
			retval=-EINVAL;
			goto	RETN;
		} */else {
			retval=-EINVAL;
			goto	RETN;
		}
	}
	real_ethhdr=(struct ethhdr *)((u8 *)ethhdr+hdr_len);
#if	0//debug
	fastbat_show_raw_data((u8 *)ethhdr, 63);
	FASTBAT_DBG_RAW_MSG("[F@STBAT] (proto=%x) (DST=%pM) (SRC=%pM)\n",
		ethhdr->h_proto, ethhdr->h_dest, ethhdr->h_source);
	FASTBAT_DBG_RAW_MSG("[F@STBAT] proto(%x)\n", real_ethhdr->h_proto);
	FASTBAT_DBG_RAW_MSG("[F@STBAT] (real) (ptype=%x) (proto=%x) (DST=%pM) (SRC=%pM)\n", 
		packettype, real_ethhdr->h_proto, real_ethhdr->h_dest, real_ethhdr->h_source);
	FASTBAT_DBG_RAW_MSG("[F@STBAT] (IDX=%u) (hdr-len=%d)\n", 
		fastbat_hash_calculate(real_ethhdr->h_source, real_ethhdr->h_dest), hdr_len);
#endif

	/* check if existence or not */
	bat=fastbat_list_search(real_ethhdr->h_source, real_ethhdr->h_dest);
	if(bat) {
#if	0//debug
		fastbat_show_list_entry(fastbat_hash_calculate(bat->src, bat->dst));
		FASTBAT_DBG_RAW_MSG("[F@STBAT] SRC/DST's exist!!! (%pM %pM)\n", 
			real_ethhdr->h_source, real_ethhdr->h_dest);
#endif
		retval=-EEXIST;
		goto	RETN;
	}
	if(atomic_read(&_fastmesh_priv.global_count) >= FAST_BAT_GLOBAL_LEN_MAX) {
		retval=-ENOBUFS;
		FASTBAT_DBG_RAW_MSG("[F@STBAT] Cache size is full!!!\n");
		goto	RETN;
	}
	bat=kzalloc(sizeof(FAST_BAT), GFP_KERNEL);
	if(bat == NULL) {
		FASTBAT_BUG_ON(1);
		retval=-ENOMEM;
		goto	RETN;
	}
	atomic_inc(&_fastmesh_priv.global_count);

	/* append into global list */
	ether_addr_copy(bat->src1, ethhdr->h_source);
	ether_addr_copy(bat->dst1, ethhdr->h_dest);
	if(ethhdr->h_proto == ETH_P_8021Q) {
		bat->vlan=1;
		hdr_len=VLAN_ETH_HLEN;
		vhdr=vlan_eth_hdr(skb);
		bat->vlan_data.h_vlan_TCI=vhdr->h_vlan_TCI;
		bat->vlan_data.h_vlan_encapsulated_proto=vhdr->h_vlan_encapsulated_proto;
		if(vhdr->h_vlan_encapsulated_proto == ETH_P_BATMAN) {
			bat->batman=1;
DISASSEMBLE:
			unicast=(struct batadv_unicast_packet *)((u8 *)vhdr+hdr_len);
			if(unicast->packet_type == BATADV_UNICAST) {
				hdr_len += sizeof(struct batadv_unicast_packet);
				bat->bat_data.u.packet_type=unicast->packet_type;
				bat->bat_data.u.version=unicast->version;
				bat->bat_data.u.ttl=unicast->ttl;
				bat->bat_data.u.ttvn=unicast->ttvn;
				bat->batman_type=BATADV_UNICAST;
				ether_addr_copy(bat->bat_data.u.dest, unicast->dest);
			/*} else if(unicast->packet_type == BATADV_UNICAST_4ADDR) {
				hdr_len += sizeof(struct batadv_unicast_4addr_packet);
				unicast_4addr=(struct batadv_unicast_4addr_packet *)unicast;
				bat->bat_data.u4.u.packet_type=unicast_4addr->u.packet_type;
				bat->bat_data.u4.u.version=unicast_4addr->u.version;
				bat->bat_data.u4.u.ttl=unicast_4addr->u.ttl;
				bat->bat_data.u4.u.ttvn=unicast_4addr->u.ttvn;
				bat->batman_type=BATADV_UNICAST_4ADDR;
				ether_addr_copy(bat->bat_data.u4.u.dest, unicast_4addr->u.dest);
				ether_addr_copy(bat->bat_data.u4.src, unicast_4addr->src);
				bat->bat_data.u4.subtype=unicast_4addr->subtype;
				bat->bat_data.u4.reserved=unicast_4addr->reserved;*/
			} else if(unicast->packet_type == ETH_P_8021Q) {
				FASTBAT_DBG_RAW_MSG(" warning! don't support vlan among vlan!\n");
				/*hdr_len += VLAN_HLEN;
				goto	DISASSEMBLE;*/
				retval=-EINVAL;
				goto	FREE;
			} else {
				FASTBAT_DBG_RAW_MSG("[FASTBAT] !BATADV_UNICAST & !BATADV_UNICAST_4ADDR but (%x)\n", 
					unicast->packet_type);
				goto	FREE;
			}
		} else {
			FASTBAT_ERR_MSG(" Error! No batman header!\n");
			goto	FREE;
		}
	} else if(ethhdr->h_proto == ETH_P_BATMAN) {
		bat->batman=1;
		hdr_len=ETH_HLEN;
		unicast=(struct batadv_unicast_packet *)(ethhdr+1);
		if(unicast->packet_type == BATADV_UNICAST) {
			hdr_len += sizeof(struct batadv_unicast_packet);
			bat->bat_data.u.packet_type=BATADV_UNICAST;//unicast->packet_type;
			bat->bat_data.u.version=unicast->version;
			bat->bat_data.u.ttl=unicast->ttl;
			bat->bat_data.u.ttvn=unicast->ttvn;
			bat->batman_type=BATADV_UNICAST;
			ether_addr_copy(bat->bat_data.u.dest, unicast->dest);
		/*} else if(unicast->packet_type == BATADV_UNICAST_4ADDR) {
			hdr_len += sizeof(struct batadv_unicast_4addr_packet);
			unicast_4addr=(struct batadv_unicast_4addr_packet *)unicast;
			bat->bat_data.u4.u.packet_type=BATADV_UNICAST_4ADDR;//unicast_4addr->u.packet_type;
			bat->bat_data.u4.u.version=unicast_4addr->u.version;
			bat->bat_data.u4.u.ttl=unicast_4addr->u.ttl;
			bat->bat_data.u4.u.ttvn=unicast_4addr->u.ttvn;
			bat->batman_type=BATADV_UNICAST_4ADDR;
			ether_addr_copy(bat->bat_data.u4.u.dest, unicast_4addr->u.dest);
			ether_addr_copy(bat->bat_data.u4.src, unicast_4addr->src);
			bat->bat_data.u4.subtype=unicast_4addr->subtype;
			bat->bat_data.u4.reserved=unicast_4addr->reserved;*/
		} else {
			FASTBAT_WARN_MSG("[F@STBAT] !BATADV_UNICAST / !BATADV_UNICAST_4ADDR but (%x)\n", 
				unicast->packet_type);
			goto	FREE;
		}
	}
	ethhdr=(struct ethhdr *)((u8 *)ethhdr + hdr_len);	/*real*/
	bat->net_dev=out;
	bat->soft_iface=soft;
	retval=fastbat_insert_list_entry(ethhdr->h_source, ethhdr->h_dest, bat);
	if(retval < 0) {
		retval=-EINVAL;
		goto	FREE;
	}
	bat->last_seen=jiffies;
	//bat->dirty=0;				//kzalloc()
	bat->iif=skb->skb_iif;		/* store input dev idx */
	ether_addr_copy(bat->src, ethhdr->h_source);
	ether_addr_copy(bat->dst, ethhdr->h_dest);
	if(neigh) {
		struct batadv_orig_node	*orig_node;
		orig_node=_br_fastbat_ops.fastbat_grab_orig_by_neigh(neigh, ethhdr->h_dest, soft);
		if(orig_node != NULL)
			ether_addr_copy(bat->orig, orig_node->orig);
		else
			ether_addr_copy(bat->orig, neigh);
	}

	FASTBAT_DBG_RAW_MSG("[F@STBAT] comes new bat (%pM)=>(%pM)!\n", bat->src, bat->dst);
	//fastbat_show_list_entry(fastbat_hash_calculate(bat->src, bat->dst));
	return	0;
FREE:
	kfree(bat);
RETN:
	return	retval;
}
EXPORT_SYMBOL(fastbat_chk_and_add_list_entry);

/* purge local dirty entry
 */
static void		fastbat_del_local_entry
(FAST_LOCAL *local)
{
    FASTBAT_DBG_RAW_MSG("[F@STBAT] dirty local bat! eliminates!\n");
    mutex_lock(&local_lock);
    list_del(local);
	atomic_dec(&_fastmesh_priv.local_count);
    mutex_unlock(&local_lock);
    kfree(local);
}

/* purge global dirty bat
 */
static void		fastbat_del_list_entry
(FAST_BAT *bat)
{
	FASTBAT_DBG_RAW_MSG("[F@STBAT] dirty bat! eliminates!\n");
	mutex_lock(&bats_lock);
	__hlist_del(&bat->list);
	atomic_dec(&_fastmesh_priv.global_count);
	mutex_unlock(&bats_lock);
	kfree(bat);
}

/* return NULL if non-exist
 * return corresponding FAST_LOCAL if found
 */
FAST_LOCAL	*fastbat_local_search
(u8 *addr)
{
	FAST_LOCAL	*local;
	list_for_each_entry(local, &fastbat_local_head, list) {
		if(memcmp(local->dst, addr, ETH_ALEN) == 0) {
			if(!local->dirty) {
				return	local;
			} else {
				//fastbat_del_local_entry(local);
				//fastbat_show_local_entry();
				break;
			}
		}
	}
	return	NULL;
}
EXPORT_SYMBOL(fastbat_local_search);

/* search from global bats
 * return NULL if failed
 * return corresponding FAST_BAT if exist
 */
FAST_BAT	*fastbat_list_search
(u8 *src, 
 u8 *dst)
{
	u32			idx=0;
	int			retval=0;
	FAST_BAT	*bat;

	/* data reading don't really need mutex to protect */
	idx=fastbat_hash_calculate(src, dst);
	//FASTBAT_DBG_RAW_MSG("[F@STBAT] INPUT(idx=%d) SRC:%pM DST:%pM\n", idx, src, dst);
	if(idx == 0xfffffffe)
		goto	RETN;
	hlist_for_each_entry(bat, &bats[idx], list) {
		//FASTBAT_DBG_RAW_MSG("[F@STBAT] ENTRY SRC:%pM DST:%pM\n", bat->src, bat->dst);
		/* filter: src and dst addr are both the same*/
		if((memcmp(bat->src, src, ETH_ALEN)==0) && (memcmp(bat->dst, dst, ETH_ALEN)==0)){
			//FASTBAT_DBG_RAW_MSG("[F@STBAT] got a blobal bat!!!\n");
			if(!bat->dirty) {
				return	bat;
			} else {
				//fastbat_del_list_entry(bat);
				//fastbat_show_list_entry(idx);
				goto	RETN;
			}
		}
	}
RETN:
	return	NULL;
}
EXPORT_SYMBOL(fastbat_list_search);

/* input skb and verdict if ETH_P_BATMAN or else
 * return ETH_P_BATMAN or else
 */
static inline u16	fastbat_grab_prototype
(struct sk_buff *skb)
{
	return  eth_hdr(skb)->h_proto;
}

/* TODO: double vlan
 * input : vlan => 0/1
 * return packet_type of batman (ELP/OGM/BCAST/UNICAST/UNICAST_4ADDR/...)
 */
static inline u8	fastbat_grab_batman_packettype
(struct ethhdr *ethhdr, 
 int vlan)
{
	u8	*packet_type;

	if(!vlan) {
		packet_type=(u8 *)ethhdr + ETH_HLEN;
	} else /*if(valn == 1)*/{
		packet_type=(u8 *)ethhdr + VLAN_ETH_HLEN;
	}/* else if (vlan == 2) {
		packet_type=(u8 *)ethhdr + VLAN_ETH_HLEN + VLAN_HLEN;
	} */
		
	return	*packet_type;
}

/* filter from forbid_mac_addr table 
 * return 0 if forbidden
 *        1 if deliver directly
 */
static u8	fastbat_verdict_phyaddr
(u8 *addr)
{
	u8	pass=1;
	u8	idx=0;

	/*if(addr == NULL) {
		FASTBAT_DBG_MSG(" Err! Null ptr!\n");
		pass=0;
		goto	RETN;
	}*/
	for(; forbid_mac_addr[idx][6] != NULL; idx++) {
		if(memcmp(&forbid_mac_addr[idx][0], addr, forbid_mac_addr[idx][6]) == 0) {
			pass=0;
			break;
		}
	}
RETN:
	return	pass;
}

/* TODO: double VLAN & 4addr
 * return NULL if !(BATADV_UNICAST/BATADV_UNICAST_4ADDR)
 * return 1st-layer ether header if !(ETH_P_8021Q/ETH_P_BATMAN)
 * return 2nd-layer ether header if ETH_P_8021Q / ETH_P_BATMAN
 */
struct ethhdr	*fastbat_grab_real_ethhdr
(struct sk_buff *skb)
{
	struct ethhdr		*eth, 
						*real_eth;
	struct vlan_ethhdr	*vhdr;
	int					hdr_len=0;
	struct batadv_unicast_packet		*u;
	//struct batadv_unicast_4addr_packet	*u4;

	eth=eth_hdr(skb);
	if(eth->h_proto == ETH_P_BATMAN) {
		u=(struct batadv_unicast_packet *)((u8 *)eth + ETH_HLEN);
		if(u->packet_type == BATADV_UNICAST)
			hdr_len=sizeof(struct batadv_unicast_packet);
		else if(u->packet_type == BATADV_UNICAST_4ADDR)
			hdr_len=sizeof(struct batadv_unicast_4addr_packet);
		else {
			FASTBAT_DBG_MSG(" warning! other types of packets!\n");
			return	NULL;
		}
		real_eth=(struct ethhdr *)((u8 *)u + hdr_len);
	} else if(eth->h_proto == ETH_P_8021Q) {
		vhdr=vlan_eth_hdr(skb);
		if(vhdr->h_vlan_encapsulated_proto != ETH_P_BATMAN) {
			return	eth;	//FIXME
		}
		u=(struct batadv_unicast_packet *)((u8 *)vhdr + VLAN_ETH_HLEN);
		if(u->packet_type == BATADV_UNICAST)
			hdr_len=sizeof(struct batadv_unicast_packet);
		else if(u->packet_type == BATADV_UNICAST_4ADDR)
			hdr_len=sizeof(struct batadv_unicast_4addr_packet);
		else {
			FASTBAT_DBG_MSG(" warning! other type packets!\n");
			return	NULL;
		}
	} else {
		//FASTBAT_DBG_RAW_MSG("[F@STBAT] (S):%pM (D):%pM", eth->h_source, eth->h_dest);
		//FASTBAT_DBG_RAW_MSG(" - protocol:0x%x\n", eth->h_proto);
		return  eth;
	}
	//FASTBAT_DBG_RAW_MSG("[F@STBAT] (RS):%pM (RD):%pM", eth->h_source, eth->h_dest);
	//FASTBAT_DBG_RAW_MSG(" - protocol:0x%x len:%d\n", eth->h_proto, hdr_len);
	real_eth=(struct ethher *)((u8 *)u + hdr_len);
	return	real_eth;
}

/* TODO: double vlan
 * 1st step: filter ARP/...
 * 2nd step: filter elp/ogm/... packet
 * 3rd step: filter forbid_mac_addr
 * return 1 => xmit directly if bat's exist
 */
int		fastbat_filter_input_packet
(struct sk_buff *skb, 
 u16 *vlan, 
 u16 *batman)
{
	int		pass=0;
	struct	vlan_ethhdr	*vhdr;
	struct	ethhdr		*ethhdr=eth_hdr(skb);

	*vlan=0;
	*batman=0;

	fastbat_show_raw_data((u8 *)ethhdr, 63);
	/* verdict proto type of ethernet */
	if((ethhdr->h_proto==ETH_P_ARP) || (ethhdr->h_proto==0x893a/*IEEE-1905*/)) {
		goto	RETN;
	} else if(ethhdr->h_proto == ETH_P_BATMAN) {
		*batman=1;
		pass=fastbat_grab_batman_packettype(ethhdr, *vlan);
	} else if(ethhdr->h_proto == ETH_P_8021Q) {
		*vlan=1;
		vhdr=vlan_eth_hdr(skb);
		/* FIXME: if batman is among vlan??? */
		if(vhdr->h_vlan_encapsulated_proto == ETH_P_BATMAN) {
			*batman=1;
			pass=fastbat_grab_batman_packettype(ethhdr, *vlan);
		} /*else if(vhdr->h_vlan_encapsulated_proto == ETH_P_8021Q) {
		} */else {
			FASTBAT_DBG_MSG(" Err! No batman header!\n");
		}
	} else if(is_broadcast_ether_addr(ethhdr->h_dest) && (ethhdr->h_proto==ETH_P_IP)) {
		/* find out dhcp request and then verdict dev idx */
		u8				*check;
		FAST_LOCAL		*local;
		struct iphdr	*iphdr;
		
		iphdr=(struct iphdr *)(ethhdr+1);
		check=(u8 *)ethhdr + ETH_HLEN + iphdr->ihl*4;
		/* len of ethernet II + ipv4 header => 14 + 20
		 * source/dest port: 68/67 (dhcp rqeuest/reply)
		 */
		if((iphdr->protocol==IPPROTO_UDP) && (*(check+1)==0x44) && (*(check+3)==0x43)) { 
			/* check if local's existed and delete it to avoid re-connection from other iface of the same device */
			local=fastbat_local_search(ethhdr->h_source);
			if(local!=NULL && local->iif!=skb->skb_iif) {
				FASTBAT_DBG_MSG("[F@STBAT] DHCP REQUEST incomes ...\n");
				/* delete original record and re-create */
				fastbat_del_local_entry(local);
			}
		}
	}
	if(pass)
		goto	RETN;

	/* verdict mac addr */
	pass=fastbat_verdict_phyaddr(ethhdr->h_dest);	//dont filter real dest mac???
RETN:
#if	(FASTBAT_DBG == 1)
	//if(pass)
		FASTBAT_DBG_RAW_MSG("[F@STBAT] (pass=%d/%04x) (vlan=%d) (batman=%d)\n", 
			pass, ethhdr->h_proto, *vlan, *batman);
	if(pass)
		fastbat_show_raw_data((u8 *)ethhdr, 63);
#endif
	return	pass;
}
EXPORT_SYMBOL(fastbat_filter_input_packet);

/* EX. 
 * pass=fastbat_filter_input_packet(skb, &vlan, &batman);
 * if(pass)
 *    bat=fastbat_list_search(ethhdr->h_source, ethhdr->dest);
 *    if(bat != NULL)
 *       fastbat_dispatch(skb, bat, vlan, batman);
 *
 * invoke fastbat_dispatch if finding out corresponding bat
 * 
 * 1. vlan + batman
 * 2. batman
 * 3. !vlan + !batman
 *
 * return 0 if successful
 */
int		fastbat_dispatch
(struct sk_buff *skb, 
 FAST_BAT *bat, 
 u16 vlan, 
 u16 batman)
{
	int	retval=-1;

	if(bat == NULL) {
		FASTBAT_DBG_MSG(" Dispatch failed!\n");
		goto	RETN;
	}
	FASTBAT_DBG_RAW_MSG("[F@STBAT] dispatch: vlan(%d)batman(%d) => vlan(%d)batman(%d)\n", 
		vlan, batman, bat->vlan, bat->batman);
	
	if(vlan && batman && bat->vlan && bat->batman)
		retval=fastbat_xmit(skb, bat);
	else if(!vlan && !batman && bat->vlan && bat->batman)
		retval=fastbat_xmit_wrap(skb, bat);
	else if(!vlan && batman && !bat->vlan && bat->batman)
		retval=fastbat_xmit(skb, bat);
	else if(!vlan && !batman && !bat->vlan && bat->batman)
		retval=fastbat_xmit_wrap(skb, bat);
	else if(!vlan && !batman && !bat->vlan && !bat->batman)
		retval=fastbat_xmit(skb, bat);
	else {
		FASTBAT_DBG_MSG(" Dispatch failed!\n");
	}

#if	(FASTBAT_DBG==1)
	if(retval < 0)
		FASTBAT_DBG_RAW_MSG("[F@STBAT] fastbat_dispatch failed!(%d)\n", retval);
	else
		FASTBAT_DBG_MSG(" fastbat_dispatch!\n");
#endif
RETN:
	return	retval;
}
EXPORT_SYMBOL(fastbat_dispatch);

/* return error code if failed
 * return 0 if successful
 */
int		fastbat_xmit_check
(FAST_BAT *bat)
{
	int	retval=0;

#if	(PUSH_THE_PACE == 1)
	/* checking input arguments */
	/*if(unlikely(bat->net_dev)) {
		retval=-ENODEV;
		goto    RETN;
	}
	if(unlikely(bat->soft_iface)) {
		retval=-ENODEV;
		goto    RETN;
	}*/
#endif
	if(!(bat->net_dev->flags & IFF_UP)) {
		retval=-EINVAL;
		goto	RETN;
	}
	if(bat->dirty) {
		FASTBAT_DBG_MSG("[F@STBAT] PASS! dirty bat!\n");
		retval=-EINVAL;
		goto	RETN;
	}
	if(!bat->batman) {
		FASTBAT_ERR_MSG(" Err! No batman header!\n");
		retval=-EEXIST;
	}
RETN:
	return	retval;
}

/* TODO: double vlan
 * wrap skb by vlan/batman header and then xmit
 */
static int	fastbat_xmit_wrap
(struct sk_buff *skb, 
 FAST_BAT *bat)
{
	int		retval=0;
	int		hdr_len=0;
	struct vlan_ethhdr	*vhdr;
	struct ethhdr		*ethhdr;

	retval=fastbat_xmit_check(bat);
	if(retval < 0)
		goto	ERR;
#if		(PUSH_THE_PACE == 1)
	if(bat->batman) {
#endif
		if(bat->batman_type == BATADV_UNICAST) {
			hdr_len=sizeof(struct batadv_unicast_packet);
		} else if(bat->batman_type == BATADV_UNICAST_4ADDR) {
			hdr_len=sizeof(struct batadv_unicast_4addr_packet);
		} else {
			FASTBAT_ERR_MSG(" Err batman type!\n");
			retval=-EINVAL;
			goto	ERR;
		}
#if     (PUSH_THE_PACE == 1)
	} else {
		retval=-EINVAL;
		FASTBAT_ERR_MSG(" Err! no batman header!\n");
		goto	ERR;
	}
#endif

	if(bat->vlan) {
		hdr_len += VLAN_ETH_HLEN; 
	} else {
		hdr_len += ETH_HLEN;
	}

#if	!(PUSH_THE_PACE == 1)
	/* move forward hdr_len bytes, and reset mac header */
	//fastbat_show_raw_data((void *)skb->data, 32);
	ethhdr=eth_hdr(skb);
	FASTBAT_DBG_RAW_MSG("[F@STBAT] push ethhdr(%p) data(%p) head(%p) mac(%p)\n", 
		ethhdr, skb->data, skb->head, skb_mac_header(skb));

	/* if not the same, alter skb's data ptr location to real ethernet header */
	if(skb->data != skb->mac_header)
		skb_push(skb, (skb->data - skb->mac_header));

	/* keep alter data's ptr to ethernet header */
	retval=_br_fastbat_ops.batadv_skb_head_push(skb, hdr_len);
	if(retval < 0) {
		FASTBAT_ERR_MSG(" batadv_skb_head_push() failed!\n");
		retval=-ENOMEM;
		goto	ERR;
	}
#else
	retval=_br_fastbat_ops.batadv_skb_head_push(skb, (skb->data - skb->mac_header) + hdr_len);
	if(retval < 0) {
		WARN_ON(1);
		goto	ERR;
	}
#endif
	skb_reset_mac_header(skb);
	//skb_set_network_header(skb, ETH_HLEN);

	if(bat->vlan) {
		vhdr=vlan_eth_hdr(skb);
		vhdr->h_vlan_TCI=bat->vlan_data.h_vlan_TCI;
		vhdr->h_vlan_encapsulated_proto=bat->vlan_data.h_vlan_encapsulated_proto;
		ether_addr_copy(vhdr->h_source, bat->src1);
		ether_addr_copy(vhdr->h_dest, bat->dst1);
		vhdr->h_vlan_proto=ETH_P_8021Q;
		//fastbat_show_raw_data((void *)skb->data, 32);
		retval=fastbat_clone_header((void *)bat, (void *)((struct vlan_ethhdr *)vhdr+1));
	} else {
		ethhdr=eth_hdr(skb);
		ether_addr_copy(ethhdr->h_dest, bat->dst1);
		ether_addr_copy(ethhdr->h_source, bat->src1);
		ethhdr->h_proto=ETH_P_BATMAN;
		//fastbat_show_raw_data((void *)skb->data, 32);
		retval=fastbat_clone_header((void *)bat, (void *)((struct ethhdr *)ethhdr+1));
	}
	if(retval < 0) {
		retval=-EINVAL;
		goto	ERR;
	}
	skb->dev=bat->net_dev;
	skb->protocol=htons(ETH_P_BATMAN);
#if	(FASTBAT_DBG == 1)
	FASTBAT_DBG_RAW_MSG("[F@STBAT] wrap xmit ethhdr(%p) data(%p) head(%p) mac(%p)\n", 
		ethhdr, skb->data, skb->head, skb_mac_header(skb));
	fastbat_show_raw_data((void *)skb->data, 63);
	FASTBAT_DBG_RAW_MSG("[F@STBAT] wrap xmit (s)%pM (d)%pM\n", ethhdr->h_source, ethhdr->h_dest);
	FASTBAT_DBG_MSG("[F@STBAT] wrap xmit(%s VS %s)!\n", skb->dev->name, bat->net_dev->name);
#endif

	/* transfer here */
	retval=dev_queue_xmit(skb);
#if	(PUSH_THE_PACE == 1)
	return	retval;
#endif
	bat->last_seen=jiffies;
	return	net_xmit_eval(retval);	/* congestion notification */
ERR:
	FASTBAT_ERR_MSG(" wrap xmit failed code(%d)\n", retval);
	return	retval;
}

/* TODO: double vlan
 * 
 * take off header of vlan/batman and then xmit skb
 */
int		fastbat_xmit_peel
(struct sk_buff *skb, 
 FAST_LOCAL *local)
{
	int		retval=-EINVAL;
	struct ethhdr	*ethhdr;
	int		vlan=0;		/* 0/1 */
	int		hdr_len=0, 
			bat_len=0;
	u16		h_proto=0;
	struct batadv_unicast_packet	*u;

	/* check insane condition */
#if	!(PUSH_THE_PACE == 1)
	if(!local->output_dev->flags & IFF_UP)
		goto	ERR;
#endif
	ethhdr=eth_hdr(skb);
	if(ethhdr->h_proto == ETH_P_BATMAN) {
		hdr_len=ETH_HLEN;
	} else if(ethhdr->h_proto == ETH_P_8021Q) {
		hdr_len=ETH_VLAN_HLEN;
	} else {
		FASTBAT_ERR_MSG(" don't accept this protocol type!\n");
		goto	ERR;
	}

	u=(struct batadv_unicast_packet *)((u8 *)ethhdr + hdr_len);
	if(u->packet_type == BATADV_UNICAST) {
		bat_len=sizeof(struct batadv_unicast_packet);
	} else if(u->packet_type == BATADV_UNICAST_4ADDR) {
		bat_len=sizeof(struct batadv_unicast_4addr_packet);
	} else {
		FASTBAT_ERR_MSG(" don't accept this packet type!\n");
		goto	ERR;
	}

	ethhdr=(struct ethhdr *)((u8 *)u + bat_len);
	/* alter data ptr to real ethernet header */
	skb_pull(skb, bat_len);
	skb_reset_mac_header(skb);

	skb->dev=local->output_dev;
	skb->protocol=htons(ethhdr->h_proto);
	//fastbat_show_raw_data((void *)skb->data, 63);
	retval=dev_queue_xmit(skb);
#if	!(PUSH_THE_PACE == 1)
	local->last_seen=jiffies;
#endif
	FASTBAT_DBG_RAW_MSG("[F@STBAT] peel xmit(%d)!\n", retval);
	return	retval;

ERR:
	FASTBAT_DBG_MSG(" Xmit of peel failed! Back to batman %p %p\n", skb->data, skb->mac_header);
	return	retval;
}
EXPORT_SYMBOL(fastbat_xmit_peel);

/* fastbat_reset_header(): correct L2/L3/L4 ptr and checksum
 * input: skb ptr from br_dev_xmit()
 * TODO: ipv6
 */
void	fastbat_reset_header
(void *d)
{
	struct sk_buff	*skb=(struct sk_buff *)d;
	struct iphdr    *iphdr;
	struct tcphdr	*tcphdr;
	struct ethhdr	*ethhdr, 
					*real_ethhdr;
	struct batadv_unicast_packet	*u;
	int		hdrlen=0, 
			batlen=0;

	ethhdr=eth_hdr(skb);
	if(ethhdr->h_proto == ETH_P_BATMAN) {
		FASTBAT_ERR_MSG(" Shouldn't wrap batman here(%x)!\n", ethhdr->h_proto);
		goto	ERR;
	}

	if(ethhdr->h_proto == ETH_P_IP) {
		iphdr=(struct iphdr *)((u8 *)skb->data);
		if((iphdr->protocol==IPPROTO_TCP) || (iphdr->protocol==IPPROTO_UDP)) {
#if	(FASTBAT_DBG == 1)
			FASTBAT_DBG_RAW_MSG("[F@STBAT] L2:\n");
			fastbat_show_raw_data((void *)ethhdr, 49);
			FASTBAT_DBG_RAW_MSG("[F@STBAT] L3:\n");
			fastbat_show_raw_data((void *)skb->data, 36);
#endif
			skb_reset_network_header(skb);
			skb_pull(skb, iphdr->ihl*4);
			skb_reset_transport_header(skb);

			/* Calculate Checksum - if dev doesn't support hw checksum, take off */
			if(skb->ip_summed == CHECKSUM_PARTIAL) {
				if(/*!(features & NETIF_F_ALL_CSUM) &&*/skb_checksum_help(skb)) {
					FASTBAT_DBG_RAW_MSG("[F@STBAT] csum(%x) failed!\n", 
						skb->data + skb->csum_offset);
					goto	ERR;
				}
			}
#if	(FASTBAT_DBG == 1)
			FASTBAT_DBG_RAW_MSG("[F@STBAT] start:%x offset:%d\n", 
				skb->csum_start, skb->csum_offset);
			FASTBAT_DBG_RAW_MSG("[F@STBAT] L4:\n");
			fastbat_show_raw_data((void *)skb->data, 33);
			FASTBAT_DBG_RAW_MSG("[F@STBAT] Ori:%x state:%x\n", skb->csum, skb->ip_summed);
#endif
			/* roll back to original location */
			skb_push(skb, iphdr->ihl*4);
		}
	}
ERR:
	return;
}
EXPORT_SYMBOL(fastbat_reset_header);

/* copy from bat->bat_data to skb
 * src: FAST_BAT *
 * dst: struct batadv_unicast_packet *
 */
static int	fastbat_clone_header
(void *src, 
 void *dst)
{
	struct batadv_unicast_packet		*u;
	struct batadv_unicast_4addr_packet	*u4;
	FAST_BAT	*bat=src;
	u8			priv_ttvn;
	int			retval=0;

#if	(PUSH_THE_PACE == 0)
	/* if soft_iface is null, cannot alter packet's ttvn */
	if(bat->soft_iface == NULL) {
		FASTBAT_ERR_MSG(" Err! Null soft_iface!\n");
		retval=-EEXIST;
		return	retval;
	}
#endif
	u=(struct batadv_unicast_packet *)dst;
#if	0
	if(u->packet_type && (bat->batman_type!=u->packet_type)) {
		FASTBAT_DBG_RAW_MSG(" Err! conflict packet type(%d VS %d)!\n", 
			u->packet_type, bat->batman_type);
		retval=-EINVAL;
		return	retval;
	}
#endif

	/* grab ttvn */
	priv_ttvn=_br_fastbat_ops.fastbat_grab_priv_ttvn(bat->soft_iface);
	FASTBAT_DBG_MSG(" TTVN=%d\n", priv_ttvn);
	if(bat->batman_type == BATADV_UNICAST) {
		u->packet_type=BATADV_UNICAST;
		u->version=bat->bat_data.u.version;
		--u->ttl;
		u->ttvn=priv_ttvn;
		ether_addr_copy(u->dest, bat->bat_data.u.dest);
	} else if(bat->batman_type == BATADV_UNICAST_4ADDR) {
		u4=(struct batadv_unicast_4addr_packet *)u;
		u4->u.packet_type=BATADV_UNICAST_4ADDR;
		u4->u.version=bat->bat_data.u4.u.version;
		--u4->u.ttl;
		u4->u.ttvn=priv_ttvn;
		ether_addr_copy(u4->u.dest, bat->bat_data.u4.u.dest);
		ether_addr_copy(u4->src, bat->bat_data.u4.src);
		u4->subtype=bat->bat_data.u4.subtype;
		u4->reserved=bat->bat_data.u4.reserved;
	} else {
		FASTBAT_ERR_MSG(" Err! Wrong packet type!\n");
		retval=-EINVAL;
	}
	return	retval;
}

/* TODO: double vlan
 * return 0 if successful
 *    A success does not guarantee the frame will be transmitted as it may be dropped due
 *    to congestion or traffic shaping
 * return: A negative errno code is returned on a failure
 *    it drops and returns NET_XMIT_DROP(which is > 0). This will not be treated as an error
 */
int		fastbat_xmit
(struct sk_buff *skb, 
 FAST_BAT *bat)
{
	u16				prototype;
	int				retval=0;
	struct ethhdr	*ethhdr;
	struct vlan_ethhdr					*vhdr;
	struct batadv_unicast_packet		*u;
	struct batadv_unicast_4addr_packet	*u4;

	retval=fastbat_xmit_check(bat);
	if(retval < 0)
		goto	ERR;
	if(bat->vlan/* == 1*/) {
		vhdr=vlan_eth_hdr(skb);
		vhdr->h_vlan_TCI=bat->vlan_data.h_vlan_TCI;
		vhdr->h_vlan_encapsulated_proto=bat->vlan_data.h_vlan_encapsulated_proto;
		skb->protocol=htons(ETH_P_8021Q);
		ether_addr_copy(vhdr->h_source, bat->src1);
		ether_addr_copy(vhdr->h_dest, bat->dst1);
		if(bat->batman)
			fastbat_clone_header((void *)bat, (void *)((struct vlan_ethhdr *)vhdr+1));
		else
			FASTBAT_DBG_MSG(" Wrong header!\n");
	} else if(bat->batman) {
		ethhdr=eth_hdr(skb);
		fastbat_clone_header((void *)bat, (void *)((struct ethhdr *)ethhdr+1));
		skb->protocol=htons(ETH_P_BATMAN);
		ether_addr_copy(ethhdr->h_source, bat->src1);
		ether_addr_copy(ethhdr->h_dest, bat->dst1);
	} else {
		ethhdr=eth_hdr(skb);
		ether_addr_copy(ethhdr->h_source, bat->src1);
		ether_addr_copy(ethhdr->h_dest, bat->dst1);
	}
	
	skb_push(skb, (skb->data - skb->mac_header));
	skb_reset_mac_header(skb);
	skb->dev=bat->net_dev;
#if		(PUSH_THE_PACE == 0)
	bat->last_seen=jiffies;
#endif

	retval=dev_queue_xmit(skb);
	FASTBAT_DBG_RAW_MSG("[F@STBAT] xmit(%d)!\n", retval);
	return	retval;

ERR:
	FASTBAT_DBG_MSG(" Xmit failed! Back to batman!\n");
	return	retval;
}

/* when batadv purge one global tt, we purge it too
 * 
 * @orig_node: an originator serving this client
 * @addr: the mac address of the client
 */
void	fastbat_check_client_and_setup_dirty
(struct batadv_orig_node *orig, 
 unsigned char *addr)
{
	int			idx=0;
	FAST_BAT	*bat;
	int			dirty=0;

	FASTBAT_DBG_RAW_MSG("[F@STBAT] setup dirty (o)%pM (c)%pM\n", orig, addr);

	/* global bats */
	for(; idx<FAST_BAT_LIST_LEN; idx++) {
		//if(!hlist_empty(&bats[idx])) {
			hlist_for_each_entry(bat, &bats[idx], list) {
				if(bat->dirty == 1)
					continue;
				if(!memcmp(bat->orig, orig->orig, ETH_ALEN-1)) {
					if(!memcmp(bat->src , addr, ETH_ALEN-1) || !memcmp(bat->dst, addr, ETH_ALEN-1)) {
						bat->dirty=1;
						dirty=1;
						FASTBAT_DBG_RAW_MSG("[F@STBAT] find out::(o)%pM\n (s)%pM\n (d)%pM\n (rs)%pM\n (rd)%pM\n", bat->orig, bat->src1, bat->dst1, bat->src, bat->dst);
						//fastbat_del_list_entry(bat);
					}
				}
			}
		//}
	}
	if(dirty) {
		queue_delayed_work(fastmesh_wq, &fastbat_work, msecs_to_jiffies(FASTBAT_DIRTY_PURGE));
	}
}

/* when batadv purge one local tt, we purge it too
 * 
 * @addr: the mac address of the client
 */
void	fastbat_check_local_client_and_setup_dirty
(u8 *addr)
{
	FAST_LOCAL  *local;
	int			dirty=0;
	FASTBAT_DBG_RAW_MSG("[F@STBAT] setup dirty (c)%pM\n", addr);
	/* local bats */
	list_for_each_entry(local, &fastbat_local_head, list) {
		if(!memcmp(local->dst, addr, ETH_ALEN)) {
			FASTBAT_DBG_RAW_MSG("[F@STBAT] find out local::(addr)%pM (dev)%s\n", 
				local->dst, local->output_dev->name);
			local->dirty=1;
			dirty=1;
		}
	}
	if(dirty) {
		queue_delayed_work(fastmesh_wq, &fastbat_work, msecs_to_jiffies(FASTBAT_DIRTY_PURGE));
	}
}

/* check if global bat exist before appending an local bat
 * 
 * @addr: local->dst ptr
 */
void    fastbat_local_add_check_global_and_setup_dirty
(u8 *addr)
{
	FAST_BAT    *bat;
	u32         idx=0;
	int         dirty=0;

	FASTBAT_DBG_RAW_MSG("[F@STBAT] setup dirty (o)%pM\n", addr);
	for(; idx<FAST_BAT_LIST_LEN; idx++) {
		hlist_for_each_entry(bat, &bats[idx], list) {
			if(bat->dirty)
				continue;
			if(!memcmp(addr, bat->dst, ETH_ALEN-1)) {
				bat->dirty=1;
				dirty=1;
				FASTBAT_DBG_RAW_MSG("[F@STBAT] find out:(o)%pM\n (s)%pM\n (d)%pM\n (rs)%pM\n (rd)%pM\n", bat->orig, bat->src1, bat->dst1, bat->src, bat->dst);
			}
		}
	}
	if(dirty)
		queue_delayed_work(fastmesh_wq, &fastbat_work, msecs_to_jiffies(FASTBAT_DIRTY_PURGE));
}

/* delete globa bat when purging neighbors from orignator
 * 
 * @addr: neigh_node ptr
 */
void	fastbat_check_disconnect_neighbor_and_setup_dirty
(u8 *addr)
{
	FAST_BAT	*bat;
	u32			idx=0;
	int			dirty=0;

	FASTBAT_DBG_RAW_MSG("[F@STBAT] setup dirty from disconnect (o)%pM\n", addr);
	for(; idx<FAST_BAT_LIST_LEN; idx++) {
		hlist_for_each_entry(bat, &bats[idx], list) {
			if(bat->dirty)
				continue;
			//if(!memcmp(addr, bat->dst1, ETH_ALEN) || !memcmp(addr, bat->src1, ETH_ALEN)) {
			if(!memcmp(addr, bat->orig, ETH_ALEN-1)) {
				bat->dirty=1;
				dirty=1;
				FASTBAT_DBG_RAW_MSG("[F@STBAT] find out:(o)%pM\n (s)%pM\n (d)%pM\n (rs)%pM\n (rd)%pM\n", bat->orig, bat->src1, bat->dst1, bat->src, bat->dst);
			}
		}
	}
	if(dirty)
		queue_delayed_work(fastmesh_wq, &fastbat_work, msecs_to_jiffies(FASTBAT_DIRTY_PURGE));
}

/* if updating routes, purge global bats
 * 
 */
void	fastbat_check_orig_and_setup_dirty
(struct batadv_orig_node *orig_node, 
 struct batadv_neigh_node *neigh_node)
{
	int			idx=0;
	FAST_BAT	*bat;
	int			dirty=0;

	FASTBAT_DBG_RAW_MSG("[F@STBAT] setup dirty (o)%pM (n)%pM\n", 
		orig_node->orig, neigh_node->addr);
	for(; idx<FAST_BAT_LIST_LEN; idx++) {
		//if(!hlist_empty(&bats[idx])) {
			hlist_for_each_entry(bat, &bats[idx], list) {
				if(bat->dirty == 1)
					continue;
				if(!memcmp(bat->orig, orig_node->orig, ETH_ALEN-1)) {
					bat->dirty=1;   /*dirty*/
					dirty=1;
					FASTBAT_DBG_RAW_MSG("[F@STBAT] find out:(o)%pM\n (s)%pM\n (d)%pM\n (rs)%pM\n (rd)%pM\n", bat->orig, bat->src1, bat->dst1, bat->src, bat->dst);
					//fastbat_del_list_entry(bat);
				}
			}
		//}
	}
	if(dirty) {
		queue_delayed_work(fastmesh_wq, &fastbat_work, msecs_to_jiffies(FASTBAT_DIRTY_PURGE));
	}

#if	0//debug
	int	oidx=0;
	int	nidx=0;
	struct batadv_neigh_node	*tmp;
	struct batadv_orig_ifinfo	*tmp_orig_ifinfo;
	struct batadv_neigh_ifinfo	*tmp_neigh_ifinfo;
	FASTBAT_DBG_RAW_MSG("[F@STBAT] (o)%pM/%x  (n)%pM/%x\n~~~~~~ ~~~~~~\n", 
		orig, orig_node, neigh, neigh_node);
	if(!hlist_empty(&orig_node->neigh_list)) {

		hlist_for_each_entry(tmp, &orig_node->neigh_list, list) {
			FASTBAT_DBG_RAW_MSG("[F@STBAT] (to)%pM  (tn)%pM\n", 
				tmp->orig_node->orig, tmp->addr);
		}

		hlist_for_each_entry(tmp_orig_ifinfo, &orig_node->ifinfo_list, list) {
			/*oidx++;
			if(tmp_orig_ifinfo->if_outgoing)
				FASTBAT_DBG_RAW_MSG("[F@STBAT] orig_ifinfo(%d) %s\n", 
					oidx, tmp_orig_ifinfo->if_outgoing->net_dev->name);
			else
				FASTBAT_DBG_RAW_MSG("[F@STBAT] orig_ifinfo(%d) iface is null \n", oidx);*/
			if(tmp_orig_ifinfo->router)
				FASTBAT_DBG_RAW_MSG("[F@STBAT] orig_ifinf (r)%x %pM\n", 
					tmp_orig_ifinfo->router, tmp_orig_ifinfo->router->addr);
			else {
				return;
				FASTBAT_DBG_RAW_MSG("[F@STBAT] orig_ifinfo router is null \n");
			}
			hlist_for_each_entry(tmp_neigh_ifinfo, &tmp_orig_ifinfo->router->ifinfo_list, list) {
				nidx++;
				if(tmp_neigh_ifinfo->if_outgoing)
					FASTBAT_DBG_RAW_MSG("[F@STBAT] neigh_ifinfo: %s(%d)\n", 
						tmp_neigh_ifinfo->if_outgoing->net_dev->name, nidx);
				else
					FASTBAT_DBG_RAW_MSG("[F@STBAT] neigh_ifinfo(%d) iface is null\n", nidx);
				FASTBAT_DBG_RAW_MSG("[F@STBAT] neigh_ifinfo throuput:%d\n", 
					tmp_neigh_ifinfo->bat_v.throughput);
			}
		}
	}
#endif
}

/* clear dirty global/local entry
 */
void	fastbat_purge_dirty_entry
(void *tmp)
{
	FAST_BAT	*bat;//, *bat_to_be_del;
	FAST_LOCAL	*local, *local_to_be_del;
	u32			idx=0;

	FASTBAT_DBG_RAW_MSG("[F@STBAT] start to purge bats ...\n");
	for(; idx<FAST_BAT_LIST_LEN; idx++) {
		hlist_for_each_entry(bat, &bats[idx], list) {
			if(bat->dirty) {
				//fastbat_del_list_entry(bat);
				FASTBAT_DBG_RAW_MSG("[F@STBAT] dirty bat! eliminates this bat!\n");
				if(bat->list.next != NULL)
					queue_delayed_work(fastmesh_wq, &fastbat_work, msecs_to_jiffies(FASTBAT_DIRTY_PURGE));
				mutex_lock(&bats_lock);
				hlist_del(&bat->list);
				atomic_dec(&_fastmesh_priv.global_count);
				mutex_unlock(&bats_lock);
				kfree(bat);
				break;
			}
		}
	}

	FASTBAT_DBG_RAW_MSG("[F@STBAT] start to purge local ...\n");
	if(!list_empty(&fastbat_local_head)) {
		list_for_each_entry(local, &fastbat_local_head, list) {
			if(local->dirty) {
				//fastbat_del_local_entry(local);
				local_to_be_del=local;
				local=local->list.prev;
				FASTBAT_DBG_RAW_MSG("[F@STBAT] dirty local bat! eliminates this bat!\n");
				mutex_lock(&local_lock);
				list_del(local_to_be_del);
				atomic_dec(&_fastmesh_priv.local_count);
				mutex_unlock(&local_lock);
				kfree(local_to_be_del);
			}
		}
	} else
		FASTBAT_DBG_RAW_MSG("[F@STBAT] local list is empty...\n");
}

int		fastbat_show_all_bats
(struct seq_file *seq, 
 void *offset)
{
	FAST_BAT	*bat;
	FAST_LOCAL	*local;
	int			idx=0, 
				counts=0;

	if(!fastmesh_enable) {
		seq_puts(seq, "[F@STBAT] fast bat is off!\n");
		return	0;
	}

	/* total count */
	seq_printf(seq, "[global size]: %d\n", atomic_read(&_fastmesh_priv.global_count));
	seq_printf(seq, "[local size]: %d\n", atomic_read(&_fastmesh_priv.local_count));
	/* global bats */
	seq_puts(seq, "(No.)   [idx] <src>             <dst>             <realsrc>         <realdst>         <orig/neigh>      <dirty> <batman> <vlan> <batman-type> <dev>\n");
	for(; idx<FAST_BAT_LIST_LEN; idx++) {
		if(!hlist_empty(&bats[idx])) {
			hlist_for_each_entry(bat, &bats[idx], list) {
				++counts;
				seq_printf(seq, "(NO-%02d) [%02d]  %pM %pM %pM %pM %pM %d       %d        %d      0x%x          %s\n", 
					counts, idx, bat->src1, bat->dst1, bat->src, bat->dst, bat->orig, 
					bat->dirty, bat->batman, bat->vlan, bat->batman_type, bat->net_dev->name);
			}
		}
	}

	/* local bats */
	counts=0;
	seq_puts(seq, "(NO.)   <dst>             <dirty> <vlan> <dev>\n");
	if(!list_empty(&fastbat_local_head)) {
		list_for_each_entry(local, &fastbat_local_head, list) {
			++counts;
			seq_printf(seq, "(NO-%02d) %pM %d       %d      %s\n", 
				counts, local->dst, local->dirty, local->vlan, local->output_dev->name);
		}
	}
	return	0;
}

/* if client change from eth0 to eth1 or from main wifi to guest wifi
 * delete local entry to keep consistency
 */
void	fastbat_update_arrival_dev
(FAST_BAT *global, 
 int iif)
{
	FAST_LOCAL	*local;
	/* update global dev idx from skb_iif */
	global->iif=iif;

	/* delete local entry */
	list_for_each_entry(local, &fastbat_local_head, list) {
		if(!memcmp(global->src, local->dst, ETH_ALEN)) {
			FASTBAT_DBG_RAW_MSG("[F@STBAT] arrival dev idx changed (%pM)\n", global->src);
			fastbat_del_local_entry(local);
			return;
		}
	}
	FASTBAT_DBG_RAW_MSG("[F@STBAT] No local!\n");
}

static int __init	fastbat_init
(void)
{
	if(&_br_fastbat_ops == NULL) {
		FASTBAT_DBG_RAW_MSG("[F@STBAT] _br_fastbat_ops is null! LEAVE!\n");
		return	-1;
	}
	if(_br_fastbat_ops.bat_priv == NULL) {
		FASTBAT_DBG_RAW_MSG("[F@STBAT] bat_priv is null! LEAVE!\n");
		return	-1;
	}

	/* initialization counters */
	atomic_set(&_fastmesh_priv.global_count, 0);
	atomic_set(&_fastmesh_priv.local_count, 0);

	/* initialization WQ */
	fastmesh_wq=create_singlethread_workqueue("fastmesh_wq");
	if(!fastmesh_wq)
		return	-1;
	INIT_DELAYED_WORK(&fastbat_work, fastbat_purge_dirty_entry);

	/* initialization others */
	if(&_br_fastbat_ops != NULL) {
		_br_fastbat_ops.fastbat_dispatch=fastbat_dispatch;
		_br_fastbat_ops.fastbat_filter_input_packet=fastbat_filter_input_packet;
		_br_fastbat_ops.fastbat_list_search=fastbat_list_search;
		_br_fastbat_ops.fastbat_grab_real_ethhdr=fastbat_grab_real_ethhdr;
		_br_fastbat_ops.fastbat_show_raw_data=fastbat_show_raw_data;
		_br_fastbat_ops.fastbat_local_search=fastbat_local_search;
		_br_fastbat_ops.fastbat_chk_and_add_local_entry=fastbat_chk_and_add_local_entry;
		_br_fastbat_ops.fastbat_reset_header=fastbat_reset_header;
		_br_fastbat_ops.fastbat_update_arrival_dev=fastbat_update_arrival_dev;
		_br_fastbat_ops.fastbat_xmit_peel=fastbat_xmit_peel;
		_br_fastbat_ops.fastbat_check_orig_and_setup_dirty=fastbat_check_orig_and_setup_dirty;
		_br_fastbat_ops.fastbat_check_disconnect_neighbor_and_setup_dirty=fastbat_check_disconnect_neighbor_and_setup_dirty;
		_br_fastbat_ops.fastbat_check_local_client_and_setup_dirty=fastbat_check_local_client_and_setup_dirty;
		_br_fastbat_ops.fastbat_check_client_and_setup_dirty=fastbat_check_client_and_setup_dirty;
		_br_fastbat_ops.fastbat_chk_and_add_list_entry=fastbat_chk_and_add_list_entry;
		_br_fastbat_ops.fastbat_show_all_bats=fastbat_show_all_bats;
		if(&_fastmesh_priv != NULL)
			_fastmesh_priv.ops=&_br_fastbat_ops;
		FASTBAT_DBG_RAW_MSG("[F@STBAT] altered callback func addr of br!\n");
	}
RETN:
	return	0;
}

static void __exit  fastbat_exit
(void)
{
	/* reclaimation */
	return;
}

MODULE_AUTHOR("Hung Feng-Jung <dononohdo@gmail.com");
MODULE_DESCRIPTION("F@STBAT Module");
MODULE_LICENSE("GPL v2");
module_init(fastbat_init);
module_exit(fastbat_exit);

