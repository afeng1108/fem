
#ifndef	FASTBAT_BATMAN_H
#define	FASTBAT_BATMAN_H

#include <linux/jhash.h>
#include <linux/list.h>
#include <linux/ratelimit.h>
#include <linux/if_vlan.h>
#include <linux/types.h>
#include <linux/etherdevice.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include "include/batman.h"
#include <linux/netdevice.h>
#include <linux/skbuff.h>

#define FASTBAT_DBG			0			/* debug mode: [0..2] */
#define PUSH_THE_PACE		1			/* speed up */
//#define ETH_ALEN			6
#define FASTBAT_DIRTY_PURGE	3000		/* msec */
#define ETH_VLAN_HLEN		18
#define FAST_BAT_LIST_LEN	32
#define FAST_BAT_GLOBAL_LEN_MAX	50000
#define FAST_BAT_LOCAL_LEN_MAX	512

//#define COLLECT_ORIG_OR_NEIGH			//disable this function on 2020/05
//#define BYPASS_GUEST_LAN				//disable this function on 2020/05

typedef struct	fastmesh_priv {
	struct batadv_priv	*batman_priv;	/* so far, just support one batman iface */
	atomic_t	global_count;			/* total global bats */
	atomic_t	local_count;			/* total local bats */
	struct hlist_head	bats[FAST_BAT_LIST_LEN];
	struct list_head	fastbat_local_head;
	struct mutex		bats_lock;
	struct mutex		local_lock;
	struct workqueue_struct	*wq;
	struct delayed_work	*work;
	struct br_fastbat_ops	*ops;
}FASTMESH;

typedef struct	fast_bat {
	struct hlist_node	list;
	u8	src1[ETH_ALEN];					//1st-layer src
	u8	dst1[ETH_ALEN];					//1st-layer dst
	u8	src[ETH_ALEN];					//real src
	u8	dst[ETH_ALEN];					//real dst
	u8	orig[ETH_ALEN];
	u8	dirty;
	unsigned long	last_seen;			//time of last handle
	int	iif;							//skb_iif
	struct net_device	*soft_iface;	//batman iface
	struct net_device	*net_dev;		//output iface
	u8	vlan;							// [0/1] => [yes/no]
	struct vlan_hdr	vlan_data;
	u8	batman;							// [0/1] => [yes/no]
	u8	batman_type;					// [0/BATADV_UNICAST/BATADV_UNICAST_4ADDR]
	union batman_header	{
		struct batadv_unicast_packet		u;
		struct batadv_unicast_4addr_packet	u4;
	}bat_data;
}FAST_BAT;

typedef struct	fast_local {
	struct list_head	list;
	u8	dst[ETH_ALEN];
	unsigned long	last_seen;
	u8	dirty;
	u8	vlan;							//on/off
	int	iif;							//skb_iif
	struct net_device	*output_dev;
}FAST_LOCAL;

struct br_fastbat_ops {
    int (*fastbat_dispatch)(struct sk_buff *skb, FAST_BAT *bat, u16 vlan, u16 batman);
    int (*fastbat_filter_input_packet)(struct sk_buff *skb, u16 *vlan, u16 *batman);
    FAST_BAT *(*fastbat_list_search)(u8 *src, u8 *dst);
    struct ethhdr *(*fastbat_grab_real_ethhdr)(struct sk_buff *skb);
    void (*fastbat_show_raw_data)(u8 *addr, u8 len);
    FAST_LOCAL *(*fastbat_local_search)(u8 *dest);
    int (*fastbat_chk_and_add_local_entry)(struct sk_buff *, struct net_device *);
    void (*fastbat_reset_header)(void *);
	void (*fastbat_update_arrival_dev)(FAST_BAT *, int);
	void (*fastbat_check_orig_and_setup_dirty)(struct batadv_orig_node *, struct batadv_neigh_node *);
	int (*fastbat_xmit_peel)(struct sk_buff *, FAST_LOCAL *);
	void (*fastbat_check_disconnect_neighbor_and_setup_dirty)(u8 *);
	void (*fastbat_check_local_client_and_setup_dirty)(u8 *);
	void (*fastbat_check_client_and_setup_dirty)(struct batadv_orig_node *, unsigned char *);
	int (*fastbat_chk_and_add_list_entry)(struct sk_buff *, struct net_device *, struct net_device *, u8 *);
	int (*fastbat_show_all_bats)(struct seq_file *, void *);
	struct batadv_orig_node *(*fastbat_grab_orig_by_neigh)(u8 *addr, u8 *real, struct net_device *soft);
	int (*fastbat_check_local_tt)(u8 *);
	u8 (*fastbat_grab_priv_ttvn)(struct net_device *);
	int (*batadv_skb_head_push)(struct sk_buff *skb, unsigned int len);
	void *bat_priv;
};

int			fastbat_dispatch(struct sk_buff *, FAST_BAT *, u16, u16);
int			fastbat_filter_input_packet(struct sk_buff *, u16 *, u16 *);
FAST_BAT	*fastbat_list_search(u8 *, u8 *);
void		fastbat_show_raw_data(u8 *, u8);
FAST_LOCAL	*fastbat_local_search(u8 *);
int			fastbat_chk_and_add_local_entry(struct sk_buff *, struct net_device *);
void		fastbat_purge_dirty_entry(void *);
void		fastbat_reset_header(void *);
#endif	//FASTBAT_BATMAN_H

