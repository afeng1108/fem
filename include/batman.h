/*******************include/uapi/asm-generic/int-l64.h*******************/
//typedef __signed__ char __s8;
//typedef unsigned char __u8;
//typedef __signed__ short __s16;
//typedef unsigned short __u16;
//typedef __signed__ int __s32;
//typedef unsigned int __u32;


/*******************include/uapi/linux/batadv_packet.h*******************/
struct batadv_unicast_packet {
    __u8 packet_type;
    __u8 version;
    __u8 ttl;
    __u8 ttvn; /* destination translation table version number */
    __u8 dest[ETH_ALEN];
    /* "4 bytes boundary + 2 bytes" long to make the payload after the
     * following ethernet header again 4 bytes boundary aligned
     */
};

struct batadv_unicast_4addr_packet {
    struct batadv_unicast_packet u;
    __u8 src[ETH_ALEN];
    __u8 subtype;
    __u8 reserved;
    /* "4 bytes boundary + 2 bytes" long to make the payload after the
     * following ethernet header again 4 bytes boundary aligned
     */
};

#define BATADV_COMPAT_VERSION 15

enum batadv_packettype {
    /* 0x00 - 0x3f: local packets or special rules for handling */
    BATADV_IV_OGM           = 0x00,
    BATADV_BCAST            = 0x01,
    BATADV_CODED            = 0x02,
    BATADV_ELP      = 0x03,
    BATADV_OGM2     = 0x04,
    /* 0x40 - 0x7f: unicast */
#define BATADV_UNICAST_MIN     0x40
    BATADV_UNICAST          = 0x40,
    BATADV_UNICAST_FRAG     = 0x41,
    BATADV_UNICAST_4ADDR    = 0x42,
    BATADV_ICMP             = 0x43,
    BATADV_UNICAST_TVLV     = 0x44,
#define BATADV_UNICAST_MAX     0x7f
    /* 0x80 - 0xff: reserved */
};


/*******************compat-include/linux/etherdevice.h*******************/
#define ether_addr_copy(_a, _b) memcpy(_a, _b, ETH_ALEN)
#define ether_addr_equal_unaligned(_a, _b) (memcmp(_a, _b, ETH_ALEN) == 0)


/*******************net/batman-adv/main.h*******************/
#define BATADV_TQ_LOCAL_WINDOW_SIZE 64
#define BATADV_FRAG_BUFFER_COUNT 8
static inline bool batadv_compare_eth(const void *data1, const void *data2)
{
    return ether_addr_equal_unaligned(data1, data2);
}


/*******************net/batman-adv/types.h*******************/
struct batadv_frag_table_entry {
    struct hlist_head fragment_list;
    spinlock_t lock;
    unsigned long timestamp;
    u16 seqno;
    u16 size;
    u16 total_size;
};

struct batadv_tt_common_entry {
    u8 addr[ETH_ALEN];
    unsigned short vid;
    struct hlist_node hash_entry;
    u16 flags;
    unsigned long added_at;
    struct kref refcount;
    struct rcu_head rcu;
};

struct batadv_tt_local_entry {
    struct batadv_tt_common_entry common;
    unsigned long last_seen;
    struct batadv_softif_vlan *vlan;
};

struct batadv_orig_bat_iv {
    unsigned long *bcast_own;
    u8 *bcast_own_sum;
    spinlock_t ogm_cnt_lock;
};

struct batadv_neigh_node {
    struct hlist_node list;
    struct batadv_orig_node *orig_node;
    u8 addr[ETH_ALEN];
    struct hlist_head ifinfo_list;
    spinlock_t ifinfo_lock;
    struct batadv_hard_iface *if_incoming;
    unsigned long last_seen;
    struct batadv_hardif_neigh_node *hardif_neigh;
    struct kref refcount;
    struct rcu_head rcu;
};

struct batadv_orig_node {
    u8 orig[ETH_ALEN];
    struct hlist_head ifinfo_list;
    struct batadv_orig_ifinfo *last_bonding_candidate;

#ifdef CONFIG_BATMAN_ADV_DAT
    batadv_dat_addr_t dat_addr;
#endif

    unsigned long last_seen;
    unsigned long bcast_seqno_reset;

#ifdef CONFIG_BATMAN_ADV_MCAST
    spinlock_t mcast_handler_lock;
    u8 mcast_flags;
    struct hlist_node mcast_want_all_unsnoopables_node;
    struct hlist_node mcast_want_all_ipv4_node;
    struct hlist_node mcast_want_all_ipv6_node;
#endif
    unsigned long capabilities;
    unsigned long capa_initialized;
    atomic_t last_ttvn;
    unsigned char *tt_buff;
    s16 tt_buff_len;
    spinlock_t tt_buff_lock;
    spinlock_t tt_lock;
    DECLARE_BITMAP(bcast_bits, BATADV_TQ_LOCAL_WINDOW_SIZE);
    u32 last_bcast_seqno;
    struct hlist_head neigh_list;
    spinlock_t neigh_list_lock;
    struct hlist_node hash_entry;
    struct batadv_priv *bat_priv;
    spinlock_t bcast_seqno_lock;
    struct kref refcount;
    struct rcu_head rcu;

#ifdef CONFIG_BATMAN_ADV_NC
    struct list_head in_coding_list;
    struct list_head out_coding_list;
    spinlock_t in_coding_list_lock;
    spinlock_t out_coding_list_lock;
#endif

    struct batadv_frag_table_entry fragments[BATADV_FRAG_BUFFER_COUNT];
    struct hlist_head vlan_list;
    spinlock_t vlan_list_lock;
    struct batadv_orig_bat_iv bat_iv;
};

struct batadv_priv_gw {
    struct hlist_head gateway_list;
    spinlock_t list_lock;
    struct batadv_gw_node __rcu *curr_gw;
    atomic_t mode;
    atomic_t sel_class;
    atomic_t bandwidth_down;
    atomic_t bandwidth_up;
    atomic_t reselect;
};

struct batadv_priv_tt {
    atomic_t vn;
    atomic_t ogm_append_cnt;
    atomic_t local_changes;
    struct list_head changes_list;
    struct batadv_hashtable *local_hash;
    struct batadv_hashtable *global_hash;
    struct hlist_head req_list;
    struct list_head roam_list;
    spinlock_t changes_list_lock;
    spinlock_t req_list_lock;
    spinlock_t roam_list_lock;
    unsigned char *last_changeset;
    s16 last_changeset_len;
    spinlock_t last_changeset_lock;
    spinlock_t commit_lock;
    struct delayed_work work;
};

struct batadv_priv_tvlv {
    struct hlist_head container_list;
    struct hlist_head handler_list;
    spinlock_t container_list_lock;
    spinlock_t handler_list_lock;
};

struct batadv_priv {
    atomic_t mesh_state;
    struct net_device *soft_iface;
    u64 __percpu *bat_counters; /* Per cpu counters */
    atomic_t aggregated_ogms;
    atomic_t bonding;
    atomic_t fragmentation;
    atomic_t packet_size_max;
    atomic_t frag_seqno;

#ifdef CONFIG_BATMAN_ADV_BLA
    atomic_t bridge_loop_avoidance;
#endif

#ifdef CONFIG_BATMAN_ADV_DAT
    atomic_t distributed_arp_table;
#endif

#ifdef CONFIG_BATMAN_ADV_MCAST
    atomic_t multicast_mode;
#endif

    atomic_t orig_interval;
    atomic_t hop_penalty;

#ifdef CONFIG_BATMAN_ADV_DEBUG
    atomic_t log_level;
#endif

    u32 isolation_mark;
    u32 isolation_mark_mask;
    atomic_t bcast_seqno;
    atomic_t bcast_queue_left;
    atomic_t batman_queue_left;
    unsigned int num_ifaces;
    struct kobject *mesh_obj;
    struct dentry *debug_dir;
    struct hlist_head forw_bat_list;
    struct hlist_head forw_bcast_list;
    struct hlist_head tp_list;
    struct batadv_hashtable *orig_hash;
    spinlock_t forw_bat_list_lock;
    spinlock_t forw_bcast_list_lock;
    spinlock_t tp_list_lock;
    atomic_t tp_num;
    struct delayed_work orig_work;
    struct batadv_hard_iface __rcu *primary_if;  /* rcu protected pointer */
    struct batadv_algo_ops *algo_ops;
    struct hlist_head softif_vlan_list;
    spinlock_t softif_vlan_list_lock;

#ifdef CONFIG_BATMAN_ADV_BLA
    struct batadv_priv_bla bla;
#endif

#ifdef CONFIG_BATMAN_ADV_DEBUG
    struct batadv_priv_debug_log *debug_log;
#endif

    struct batadv_priv_gw gw;
    struct batadv_priv_tt tt;
    struct batadv_priv_tvlv tvlv;

#ifdef CONFIG_BATMAN_ADV_DAT
    struct batadv_priv_dat dat;
#endif

#ifdef CONFIG_BATMAN_ADV_MCAST
    struct batadv_priv_mcast mcast;
#endif

#ifdef CONFIG_BATMAN_ADV_NC
    atomic_t network_coding;

    struct batadv_priv_nc nc;
#endif /* CONFIG_BATMAN_ADV_NC */

#ifdef CONFIG_BATMAN_ADV_BATMAN_V
    struct batadv_priv_bat_v bat_v;
#endif
};


/*******************net/batman-adv/hash.h*******************/
struct batadv_hashtable {
    struct hlist_head *table;
    spinlock_t *list_locks;
    u32 size;
};


